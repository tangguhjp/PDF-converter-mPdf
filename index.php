<?php

    include 'config.php';
    $cus = [];
    $sql='SELECT * FROM customer';
    $results=mysqli_query($conn,$sql);
    while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
        $cus[] = $row;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Print Invoice</title>
</head>
<body>

<form method="post" action="invoice-convert.php">
    <select name="print_option">
        <?php
            for ($i = 0; $i < sizeof($cus); $i++){
                echo '
                <option value="'.$cus[$i]['id'].'">'.$cus[$i]['name'].'</option>';
            }
        ?>
    </select>
    <input type="submit" value="Cetak"/>
</form>
</body>
</html>
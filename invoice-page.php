<?php
    include 'config.php';
    include 'invoice-config.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Print Invoice</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family:Arial;
            font-size:10pt;
            margin:0;
            padding:0;
        }

        p
        {
            margin:0;
            padding:0;
        }

        #wrapper
        {
            width:180mm;
            margin:0 15mm;
        }

        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }

        table
        {
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

        }

        table td
        {
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 2mm;
        }

        table.heading
        {
            height:50mm;
        }

        h1.heading
        {
            font-size:14pt;
            color:#000;
            font-weight:normal;
        }

        h2.heading
        {
            font-size:9pt;
            color:#000;
            font-weight:normal;
        }

        hr
        {
            color:#ccc;
            background:#ccc;
        }

        #invoice_body
        {
            height: 149mm;
        }

        #invoice_body , #invoice_total
        {
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            border-spacing:0;
            border-collapse: collapse;

            margin-top:5mm;
        }

        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding:2mm 0;
        }

        #invoice_body table td.mono  , #invoice_total table td.mono
        {
            font-family:monospace;
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }

        #footer
        {
            width:180mm;
            margin:0 15mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;

            background:#eee;

            border-spacing:0;
            border-collapse: collapse;
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <p style="text-align:center; font-weight:bold; padding-top:5mm;">FAKTUR</p>
    <br />
    <table class="heading" style="width:100%;">
        <tr>
            <td style="width:80mm;">
                <h1 class="heading">Sumber Group</h1>
                <h2 class="heading">
                    Jl. Kramat Gantung No.75,<br />
                    Alun-alun Contong, Bubutan,<br />
                    Kota SBY, Jawa Timur 60174<br />

                    Website : www.sgroup.com<br />
                    E-mail : SG@gmail.com<br />
                    Telp. : +62351-22119
                </h2>
            </td>
            <td rowspan="2" valign="top" align="right" style="padding:3mm;">
                <table>
                    <tr><td>Nomor Faktur : </td><td><?php echo date("his")?> </td></tr>
                    <tr><td>Tanggal : </td><td><?php echo date("Y-m-d") ?></td></tr>
                    <tr><td>Mata Uang : </td><td>Rupiah</td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <b>Pembeli</b> :<br />
                <?php echo $data[0]['name'] .'<br />'.
                    $data[0]['add']
                .'<br />'.
                    $data[0]['telp'].'<br />'?>
            </td>
        </tr>
    </table>


    <div id="content">

        <div id="invoice_body">
            <table>
                <tr style="background:#eee;">
                    <td style="width:8%;"><b>No.</b></td>
                    <td><b>Barang</b></td>
                    <td style="width:15%;"><b>Jumlah</b></td>
                    <td style="width:15%;"><b>Harga Satuan</b></td>
                    <td style="width:15%;"><b>Total</b></td>
                </tr>
            </table>

            <table>
                <?php
                $total = 0;
                for($i = 0; $i < sizeof($product); $i++){
                    echo '
                    <tr>
                        <td style="width:8%;">'.$i.'</td>
                        <td style="text-align:left; padding-left:10px;">'.$product[$i]['des'].'</td>
                        <td class="mono" style="width:15%;">'.$product[$i]['quan'].'</td><td style="width:15%;" class="mono">'.$product[$i]['rate'].'</td>
                        <td style="width:15%;" class="mono">'.$product[$i]['quan']*$product[$i]['rate'].'</td>
                    </tr>';
                    $total = $total + $product[$i]['quan']*$product[$i]['rate'];
                }
                ?>

                <tr>
                    <td colspan="3"></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                    <td>Total :</td>
                    <td class="mono"><?php echo $total ?></td>
                </tr>
            </table>
        </div>

        <hr />

        <table style="width:100%; height:35mm;">
            <tr>
                <td style="width:65%;" valign="top">
                    Informasi Pembayaran :<br />
                    Harap lakukan pembayaran pembayaran cek ke:<br />
                    <b>Sumber Group</b>
                    <br /><br />
                    Faktur terhutang dalam 7 hari setelah diterbitkan.
                    <br /><br />
                </td>
                <td>
                    <div id="box">
                        Petugas Kasir<br />
                        Sumber Group<br /><br /><br /><br />
                        Tanda Tangan
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <br />

</div>
</body>
</html>